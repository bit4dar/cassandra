

mkdir -p /mnt/dd

size=8
count=256000
iterations=8
i=1
while [ "$i" -le "$iterations" ]
do
	echo "iteration:$i count:$count size:$size"
	dd if=/dev/zero of=/mnt/dd/${count}.${size}.dat bs=${size}k count=${count} conv=fsync oflag=sync
	i=$(($i+1))
	size=$(($size*2))
	count=$(($count/2))
done

