#!/bin/bash

export DC_WEST="23.99.2.127,23.99.8.137,23.99.14.48"
export DC_CENTRAL="104.43.172.183,104.43.171.155,104.43.171.157"

export ALL_NODES="$DC_WEST","$DC_CENTRAL"
export SEED_NODES="$( cut -d ',' -f1-2 <<< $DC_WEST)","$( cut -d ',' -f1-2 <<< $DC_CENTRAL)"

echo "NODES:"
echo "------------------------"
echo "DC_WEST     $DC_WEST"
echo "DC_CENTRAL  $DC_CENTRAL"
echo "ALL_NODES   $ALL_NODES"
echo "SEED_NODES  $SEED_NODES"
echo "------------------------"



