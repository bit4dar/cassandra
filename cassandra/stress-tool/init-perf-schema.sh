#!/bin/bash

. ./NODES.sh

KEYSPACE_NAME="keyspace1"

CQL_NODE="$( cut -d ',' -f 1 <<< $DC_WEST)"
echo "Using Node        : $CQL_NODE"
echo "Dropping keyspace ..."
cqlsh -e "DROP KEYSPACE IF EXISTS $KEYSPACE_NAME" $CQL_NODE
echo "Starting stress to create one row ..."
cassandra-stress write n=1 -node $CQL_NODE
echo "Altering keyspace : $KEYSPACE_NAME"
cqlsh -e "ALTER KEYSPACE $KEYSPACE_NAME WITH REPLICATION = {'class' : 'NetworkTopologyStrategy', 'westus' : 2, 'eastus' : 2};" $CQL_NODE
echo "Done"

