#!/bin/bash

. ./NODES.sh

if ! [ "$1" -eq "$1" ] 2> /dev/null
then
	echo "need number of rows as parm"
	exit
fi

echo "Using nodes: $DC_WEST and number of rows: $1"
cassandra-stress write n=$1 cl=EACH_QUORUM -node $DC_WEST

