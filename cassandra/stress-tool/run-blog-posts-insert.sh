#!/bin/bash

. ./NODES.sh

cassandra-stress user profile=./blog-posts.yaml ops\(singlepost=2,timeline=1,insert=1\) cl=LOCAL_QUORUM -rate threads\>=16 threads\<=32 -node $SEED_NODES

# ops\(singlepost=2,timeline=1,insert=1\)

