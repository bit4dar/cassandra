#!/bin/bash

. ./NODES.sh

if ! [ "$1" -eq "$1" ] 2> /dev/null
then
	echo "need number seconds as parm"
	exit
fi

cassandra-stress read duration="$1s" cl=ALL no-warmup -node $DC_CENTRAL -rate threads=128

